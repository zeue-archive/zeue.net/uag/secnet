/*
	secnet.armapmc.com
	  - has a cool discord bot too uwu
*/


// [DISCORD]
	
	const fs = require('fs');
	const botToken = fs.readFileSync("./botToken.txt", {"encoding": "utf-8"});

	const Discord = require('discord.js');
	const moment = require('moment');
	const client = new Discord.Client();

	client.on('ready', () => {
		
		console.log(`Logged in as ${client.user.tag}!`);
		
		var zulu1 = moment.utc();
		if (zulu1.day() == 0) {
			var diff = zulu1.date() - zulu1.day() - 7;
		} else {
			var diff = zulu1.date() - zulu1.day() - 1;
		} diff += 7;
		zulu1.date(diff);
		zulu1.hours(17);
		zulu1.minutes(30);
		zulu1.seconds(0);
		zulu1.milliseconds(0);
		
		const self = client;
		
		setInterval(function(){
			
			var local2 = new Date().getTime();
			var off2 = new Date().getTimezoneOffset();
			var zulu2 = new Date(local2+off2);

			var distance = zulu1 - zulu2;

			var days = Math.floor(distance / (1000 * 60 * 60 * 24));
			var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
			var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
			var seconds = Math.floor((distance % (1000 * 60)) / 1000);
			
			if (distance < 0) {
				self.user.setGame('EXPIRED');
			} else {
				self.user.setGame("in " + days + "D " + hours + "H " + minutes + "M");
			}
			
		}, 30000);
		
	});

	client.login(botToken);

// [/DISCORD]

// [WEB]

	const express = require('express');

	const app = express();

	app.get('/', (req, res) => {
	  res.status(200).send('Hello, world!').end();
	});

	// Start the server
	const PORT = process.env.PORT || 8080;
	app.listen(PORT, () => {
	  console.log(`App listening on port ${PORT}`);
	  console.log('Press Ctrl+C to quit.');
	});

// [WEB]
